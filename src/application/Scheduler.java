package application;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Scheduler {
	private LinkedList<Interval> intervals = new LinkedList<Scheduler.Interval>();

    public void addTask(Task task, int position) {
        if(position<0){
            throw new IllegalArgumentException();
        }
        while(intervals.size() <= position){
            intervals.add(new Interval());
        }
        Interval interval = intervals.get(position);
        interval.add(task);
    }
	
	public class Interval {
		private List<Runnable> tasks = new ArrayList<Runnable>();

		public void add(Task task) {
			
		} 
	}
}
